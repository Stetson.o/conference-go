from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json, requests

def getpicture(city, state):
    HEADER = {
        'authorization': PEXELS_API_KEY
    }
    geturl = f"https://api.pexels.com/v1/search?query={state}%20{city}&per_page=1"
    response = requests.get(geturl, headers=HEADER)
    tmp = json.loads(response.content)
    return tmp['photos'][0]['src']['original']

def getweatherdata(city, state):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url)
    tmp = json.loads(response.content)

    lat = tmp[0]["lat"]
    lon = tmp[0]["lon"]

    weatherurl = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    weatherresponce = requests.get(weatherurl)
    tmp2 = json.loads(weatherresponce.content)
    weather = {
        "weather": tmp2["weather"][0]["main"],
        "temp": tmp2["main"]["temp"],
    }
    return weather
