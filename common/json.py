from json import JSONEncoder
from datetime import datetime
from typing import Any
from django.db.models import QuerySet

class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)

class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)

class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        if isinstance(o, self.model):
            result = {}
            if hasattr(o, "get_api_url"):
                result["href"] = o.get_api_url()
            for prop in self.properties:
                value = getattr(o, prop)
                if prop in self.encoders:
                    encoder = self.encoders[prop]
                    value = encoder.default(value)
                result[prop] = value
            result.update(self.get_extra_data(o))
            return result
        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}
